﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : MonoBehaviour
{
    private CharacterController m_LocalCharacterController = null;

    private float m_VerticalMovementSpeed = 2f;
    private float m_CurrentMovementSpeed = 0f;

    private float m_ResetXLocation = 0f;

    void Awake()
    {
        m_ResetXLocation = transform.position.x;
        m_LocalCharacterController = GetComponent<CharacterController>();        
    }

    void Update()
    {
        GetInput();

        if (m_CurrentMovementSpeed != 0)
        {
            MoveObject();
        }    
    }

    private void GetInput()
    {
        m_CurrentMovementSpeed = Input.GetAxis("Vertical") * m_VerticalMovementSpeed;
    }

    private void MoveObject()
    {
        m_LocalCharacterController.Move(new Vector3(0, m_CurrentMovementSpeed, 0) * Time.deltaTime);
        
        //Next line ensures the object stays at the same x position.
        transform.position = new Vector3(m_ResetXLocation, transform.position.y, transform.position.z);
    }
}
