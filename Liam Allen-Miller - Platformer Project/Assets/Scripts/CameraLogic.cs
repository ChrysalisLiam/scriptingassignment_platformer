﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLogic : MonoBehaviour
{
    [SerializeField] private Transform m_PlayerTransform = null;

    private static CameraLogic instance;
    public static CameraLogic Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<CameraLogic>();
            }
            return instance;
        }
    }
    void LateUpdate()
    {
        if (m_PlayerTransform != null)
        {
            transform.position = new Vector3(m_PlayerTransform.position.x, transform.position.y, transform.position.z);
        }
    }

    public void NewPlayer(Transform newPlayerTransform)
    {
        m_PlayerTransform = newPlayerTransform;
    }
}
