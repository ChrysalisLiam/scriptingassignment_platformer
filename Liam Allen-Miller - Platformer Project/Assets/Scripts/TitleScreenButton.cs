﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreenButton : MonoBehaviour
{
    [SerializeField] private string m_SceneToLoad = null;
    [SerializeField] private GameObject m_FadeObject = null;

    private bool m_buttonClicked = false;
    private Button m_LocalButtonComponent = null;

    private void Awake()
    {
        m_LocalButtonComponent = GetComponent<Button>();
        if (m_FadeObject != null)
        {
            m_FadeObject.SetActive(false);
        }
    }

    public void OnClickStartGame()
    {
        if (m_buttonClicked == false)
        {
            StartGame();
        }
    }
    private void StartGame()
    {
        SceneManager.LoadScene(m_SceneToLoad);
    }

    public void OnClickHowToPlay()
    {
        m_FadeObject.SetActive(true);
    }
}