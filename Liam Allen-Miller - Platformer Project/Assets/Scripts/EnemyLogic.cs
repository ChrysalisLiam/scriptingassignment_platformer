﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic : CharacterMovement
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerLogic>().KillPlayer();
        }
    }

    void Awake()
    {
        m_LocalAnimator = GetComponent<Animator>();
    }

    public void KillEnemy()
    {
        m_LocalAnimator.SetBool("Death", true);
        Instantiate(m_DeathParticle, gameObject.transform.position, Quaternion.identity, null);
        Invoke("ActuallyKillEnemy", 0.5f);
    }

    public void ActuallyKillEnemy()
    {  
        Destroy(gameObject);
    }
}
