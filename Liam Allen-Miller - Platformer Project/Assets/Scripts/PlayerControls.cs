﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : CharacterMovement
{
    void Awake()
    {
        m_LocalAnimator = GetComponent<Animator>();
        m_localCharacterController = GetComponent<CharacterController>();
        m_CurrentState = CharacterState2D.Standing;
    }

    void Update()
    {
        GetInput();
        RunState();
    }

    private void GetInput()
    {
        m_MovementInput.x = Input.GetAxis("Horizontal") * m_MovementSpeed;

        if (m_localCharacterController.isGrounded)
        {
            if (m_MovementInput.x == 0)
            {
                m_LocalAnimator.SetBool("Walking", false);
            }
            else
            {
                m_LocalAnimator.SetBool("Walking", true);
            }
            m_CurrentState = CharacterState2D.Moving;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_LocalAnimator.SetBool("Walking", false);
                m_CurrentState = CharacterState2D.StartJumping;
            }
        }
        else
        {
            ApplyGravity();
            m_CurrentState = CharacterState2D.Falling;
        }
    }

    private void ApplyGravity()
    {
        m_MovementInput.y -= m_Gravity * Time.deltaTime;
        if (m_MovementInput.y < -m_Gravity)
        {
            m_MovementInput.y = -m_Gravity;
        }
    }
}
