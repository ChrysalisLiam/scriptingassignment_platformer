﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Unity.Profiling;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public enum CharacterState2D
    {
        Standing,
        Moving,
        StartJumping,
        Jumping,
        Falling,
        Landing,
        Death
    }

    public CharacterController m_localCharacterController = null;
    public Animator m_LocalAnimator = null;

    public GameObject m_DeathParticle = null;

    public CharacterState2D m_CurrentState = CharacterState2D.Standing;

    public float m_MovementSpeed = 5f;
    public float m_Gravity = 9f;
    public float m_JumpForce = 10;
    public Vector3 m_MovementInput = new Vector3();

    //Excecutes actions according to currentState.
    public void RunState()
    {
        if (m_CurrentState == CharacterState2D.StartJumping)
        {
            StartJumping();
        }
        else if (m_CurrentState == CharacterState2D.Jumping)
        {
            Jumping();
        }
        else if (m_CurrentState == CharacterState2D.Falling)
        {
            Falling();
        }
        else if (m_CurrentState == CharacterState2D.Moving)
        {
            Moving();
        }
    }

    //Moves the character according to movementInput and movementSpeed.
    private void Moving()
    {
        m_localCharacterController.Move(m_MovementInput * Time.deltaTime);
    }

    //Begins a jump using m_JumpForce.
    private void StartJumping()
    {
        m_MovementInput.y = m_JumpForce;
        m_localCharacterController.Move(m_MovementInput * Time.deltaTime);

        m_CurrentState = CharacterState2D.Jumping;
    }

    //Continues jump until player begins falling.
    private void Jumping()
    {
        m_localCharacterController.Move(m_MovementInput * Time.deltaTime);

        if (m_MovementInput.y >= 0)
        {
            m_CurrentState = CharacterState2D.Falling;
        }
    }

    //Applies gravity to the character according to m_Gravity.
    private void Falling()
    {
        m_localCharacterController.Move(m_MovementInput * Time.deltaTime);
    }
}
