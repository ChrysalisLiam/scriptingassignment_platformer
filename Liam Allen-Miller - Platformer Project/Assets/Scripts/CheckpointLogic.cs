﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointLogic : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("New checkpoint set");
            GameManager.Instance.SetNewCheckpoint(gameObject.transform);
        }
    }
}
