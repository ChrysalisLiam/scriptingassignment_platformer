﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Transform m_LastCheckpoint = null;
    public GameObject m_PlayerPrefab = null;
    public int m_PlayerLives = 3;
    public string m_SceneToLoad = null;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    public void SetNewCheckpoint(Transform newCheckpoint)
    {
        m_LastCheckpoint = newCheckpoint;
    }

    public void RespawnPlayer()
    {
        if (m_PlayerLives > 0)
        {
            m_PlayerLives -= 1;
            Debug.Log("Lives remaining: " + m_PlayerLives);
            Instantiate(m_PlayerPrefab, m_LastCheckpoint.position, Quaternion.identity, null);
        }
        else
        {
            GameEndLoss();
        }
    }

    public void GameEndVictory()
    {
        Debug.Log("player wins");
        SceneManager.LoadScene(m_SceneToLoad);
    }

    public void GameEndLoss()
    {
        Debug.Log("player loses");
        SceneManager.LoadScene("Title Screen");
    }
}
