﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControls : CharacterMovement
{
    private CharacterController m_LocalCharacterController = null;

    void Awake()
    {
        m_localCharacterController = GetComponent<CharacterController>();
        m_CurrentState = CharacterState2D.Moving;
        m_MovementInput = new Vector3(-m_MovementSpeed, 0, 0);
    }

    void Update()
    {
        ApplyGravity();
        RunState();
    }

    private void ApplyGravity()
    {
        m_MovementInput.y -= m_Gravity * Time.deltaTime;
        if (m_MovementInput.y < -m_Gravity)
        {
            m_MovementInput.y = -m_Gravity;
        }
    }

    public void InvertMovement()
    {
        m_MovementInput.x = -m_MovementInput.x;
        ApplyGravity();
    }

    public void Jump()
    {
        m_CurrentState = CharacterState2D.StartJumping;
    }
}
