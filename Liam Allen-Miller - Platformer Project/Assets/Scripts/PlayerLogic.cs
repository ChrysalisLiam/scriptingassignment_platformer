﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : CharacterMovement
{
    private void Awake()
    {
        CameraLogic.Instance.NewPlayer(transform);
    }

    public void KillPlayer()
    {
        CharacterMovement m_LocalPlayerControls = GetComponent<PlayerControls>();
        m_LocalPlayerControls.m_LocalAnimator.SetBool("Death", true);
        Instantiate(m_DeathParticle, gameObject.transform.position, Quaternion.identity, null);
        Invoke("ActuallyKillPlayer", 0.5f);
    }

    public void ActuallyKillPlayer()
    {
        GameManager.Instance.RespawnPlayer();
        Destroy(gameObject);
    }
}
